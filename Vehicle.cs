﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mix_test
{
    public class Vehicle
    {
        public int ID;
        public string Registration;
        public float Latitude;
        public float Longitude;
        public DateTime RecordedTimeUTC;

        internal byte[] GetBytes()
        {
            List<byte> byteList = new List<byte>();
            byteList.AddRange((IEnumerable<byte>)BitConverter.GetBytes(this.ID));
            byteList.AddRange((IEnumerable<byte>)VehicleService.ToNullTerminatedString(this.Registration));
            byteList.AddRange((IEnumerable<byte>)BitConverter.GetBytes(this.Latitude));
            byteList.AddRange((IEnumerable<byte>)BitConverter.GetBytes(this.Longitude));
            byteList.AddRange((IEnumerable<byte>)BitConverter.GetBytes(VehicleService.ToCTime(this.RecordedTimeUTC)));
            return byteList.ToArray();
        }

        internal string GetTextSummary() => string.Format("{0}, {1}, {2}, {3}, {4}", this.ID, this.Registration, this.RecordedTimeUTC, this.Latitude, this.Longitude);

        internal static Vehicle FromBytes(byte[] buffer, ref int offset)
        {
            Vehicle vehiclePosition = new Vehicle();
            vehiclePosition.ID = BitConverter.ToInt32(buffer, offset);
            offset += 4;
            StringBuilder stringBuilder = new StringBuilder();
            while (buffer[offset] != (byte)0)
            {
                stringBuilder.Append((char)buffer[offset]);
                ++offset;
            }
            vehiclePosition.Registration = stringBuilder.ToString();
            ++offset;
            vehiclePosition.Latitude = BitConverter.ToSingle(buffer, offset);
            offset += 4;
            vehiclePosition.Longitude = BitConverter.ToSingle(buffer, offset);
            offset += 4;
            ulong uint64 = BitConverter.ToUInt64(buffer, offset);
            vehiclePosition.RecordedTimeUTC = VehicleService.FromCTime(uint64);
            offset += 8;
            return vehiclePosition;
        }
    }
}
