﻿using System;

namespace mix_test
{
    class Program
    {
        private const int VEHICLE_COUNT = 2000000;
        private const int FIND_VEHICLES = 10;
        static void Main(string[] args)
        {
            VehicleService.FindClosestN(VehicleService.GetPositions());
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
