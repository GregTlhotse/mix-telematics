﻿using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace mix_test
{
    public class VehicleService
    {
        public static Data[] GetPositions()
        {
            Data[] position = new Data[10];
            position[0].Latitude = 34.54491f;
            position[0].Longitude = -102.100845f;
            position[1].Latitude = 32.3455429f;
            position[1].Longitude = -99.12312f;
            position[2].Latitude = 33.2342339f;
            position[2].Longitude = -100.214127f;
            position[3].Latitude = 35.19574f;
            position[3].Longitude = -95.3489f;
            position[4].Latitude = 31.89584f;
            position[4].Longitude = -97.78957f;
            position[5].Latitude = 32.89584f;
            position[5].Longitude = -101.789574f;
            position[6].Latitude = 34.1158371f;
            position[6].Longitude = -100.225731f;
            position[7].Latitude = 32.33584f;
            position[7].Longitude = -99.99223f;
            position[8].Latitude = 33.53534f;
            position[8].Longitude = -94.79223f;
            position[9].Latitude = 32.2342339f;
            position[9].Longitude = -100.222221f;
            return position;
        }
        public static void FindClosestN(Data[] data)
        {
            List<Vehicle> positionList = new List<Vehicle>();
            Stopwatch stop = Stopwatch.StartNew();
            List<Vehicle> vehiclePositions = ReadDataFile();
            stop.Stop();
            long elapsedMilliseconds = stop.ElapsedMilliseconds;
            stop.Restart();
            foreach (Data coord in data)
            {
                positionList.Add(GetDistance(vehiclePositions, coord.Latitude, coord.Longitude, out double _));
            }
                
            stop.Stop();
            Console.WriteLine(string.Format("Data file read execution time : {0} ms", elapsedMilliseconds));
            Console.WriteLine(string.Format("Closest position calculation execution time : {0} ms", stop.ElapsedMilliseconds));
            Console.WriteLine(string.Format("Total execution time : {0} ms", (elapsedMilliseconds + stop.ElapsedMilliseconds)));
            Console.WriteLine();
        }

        public static void FindClosest(float latitude, float longitude)
        {
            Stopwatch stop = Stopwatch.StartNew();
            double nearestDistance;
            Vehicle nearest = GetDistance(ReadDataFile(), latitude, longitude, out nearestDistance);
            stop.Stop();
            Console.WriteLine(string.Format("Execution time : {0} ms", stop.ElapsedMilliseconds));
            Console.WriteLine(string.Format("{0} : {1}", nearestDistance, nearest.GetTextSummary()));
        }

        public static void FindClosest(int count, float latitude, float longitude)
        {
            Stopwatch stop = Stopwatch.StartNew();
            SortedList<double, Vehicle> sortedList = AddNearestPositions(ReadDataFile(), count, latitude, longitude);
            stop.Stop();
            Console.WriteLine(string.Format("Execution time : {0} ms", stop.ElapsedMilliseconds));
            foreach (double key in sortedList.Keys)
            {
                Console.WriteLine(string.Format("{0} : {1}", key, sortedList[key].GetTextSummary()));
            }
                
        }

        public static Vehicle GetDistance(List<Vehicle> vehiclePositions, float latitude, float longitude,out double nearestDistance)
        {
            Vehicle nearest = null;
            nearestDistance = double.MaxValue;
            foreach (Vehicle vehiclePosition in vehiclePositions)
            {
                double num = new GeoCoordinate(latitude, longitude).GetDistanceTo(new GeoCoordinate(vehiclePosition.Latitude, vehiclePosition.Longitude));
                
                if (num < nearestDistance)
                {
                    nearest = vehiclePosition;
                    nearestDistance = num;
                }
            }
            return nearest;
        }

        private static SortedList<double, Vehicle> AddNearestPositions(List<Vehicle> vehiclePositions, int count,float latitude,float longitude)
        {
            SortedList<double, Vehicle> sortedList = new SortedList<double, Vehicle>();
            foreach (Vehicle vehiclePosition in vehiclePositions)
            {
                double key1 =new GeoCoordinate(latitude, longitude).GetDistanceTo(new GeoCoordinate(vehiclePosition.Latitude, vehiclePosition.Longitude));
                if (sortedList.Count < count)
                {
                    sortedList.Add(key1, vehiclePosition);
                }
                else
                {
                    double key2 = sortedList.Keys[sortedList.Keys.Count - 1];
                    if (key1 < key2)
                    {
                        sortedList.Remove(key2);
                        sortedList.Add(key1, vehiclePosition);
                    }
                }
            }
            return sortedList;
        }
        public static List<Vehicle> ReadDataFile()
        {
            byte[] data = ReadFileData();
            List<Vehicle> vehiclePositionList = new List<Vehicle>();
            int offset = 0;
            if (offset < data.Length)
            {
                vehiclePositionList.Add(ReadVehiclePosition(data, ref offset));
                
            }
            return vehiclePositionList;
        }
        public static byte[] ReadFileData()
        {
            string localFilePath = GetLocalFilePath("VehiclePositions.dat");
            if (File.Exists(localFilePath))
            {
                return File.ReadAllBytes(localFilePath);
            }
                
            Console.WriteLine("Data file not found.");
            return null;
        }

        private static Vehicle ReadVehiclePosition(byte[] data, ref int offset) => Vehicle.FromBytes(data, ref offset);
        public static DateTime Epoch => new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static string GetLocalFilePath(string fileName) => GetLocalFilePath(string.Empty, fileName);

        public static string GetLocalFilePath(string subDirectory, string fileName) => Path.Combine(GetLocalPath(subDirectory), fileName);

        public static byte[] ToNullTerminatedString(string registration)
        {
            byte[] bytes = Encoding.Default.GetBytes(registration);
            byte[] terminatedString = new byte[bytes.Length + 1];
            bytes.CopyTo(terminatedString, 0);
            return terminatedString;
        }

        public static string GetLocalPath(string subDirectory)
        {
            string path1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (subDirectory != string.Empty)
                path1 = Path.Combine(path1, subDirectory);
            return path1;
        }

        public static string SelectRandom(Random rnd, string[] values)
        {
            int index = rnd.Next(values.Length - 1);
            return values[index];
        }

        public static ulong ToCTime(DateTime time) => Convert.ToUInt64((time - Epoch).TotalSeconds);

        public static DateTime FromCTime(ulong cTime) => Epoch.AddSeconds(cTime);
    }
}
